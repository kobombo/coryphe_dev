package com.dominion.coryphe.starter;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.dominion.coryphe.R;
import com.dominion.coryphe.bluetooth.BlueToothController;
import com.dominion.coryphe.bus.OttoBus;
import com.dominion.coryphe.events.CorypheShutDown;
import com.dominion.coryphe.notification.NotificationController;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;

import java.util.logging.Logger;

/**
 * @author dotreba
 */
@EActivity(R.layout.main)
public class StartActivity extends Activity {

    private static final int WAIT_TIME = 7000;

    @Bean
    OttoBus bus;
    @Bean
    BlueToothController btController;
    @Bean
    NotificationController notificationController;

    @ViewById(R.id.myInput)
    EditText myInput;

    @ViewById(R.id.myTextView)
    TextView textView;

    @AfterInject
    void init() {
        bus.register(this);
        btController.startBT();
        waitForConnection();
    }

    @Click
    void myButton() {
        String name = myInput.getText().toString();
        textView.setText("Hello " + name);
    }

    @Background
    void waitForConnection() {
        try {
            Thread.sleep(WAIT_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (!btController.isDeviceFound()) {
                btController.stopBT();
            }
        }
    }

    @Receiver(actions = BluetoothAdapter.ACTION_STATE_CHANGED)
    protected void onBlueToothStateChanged() {
        btController.checkBtStatus();
    }

    @Receiver(actions = BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)
    protected void onBlueToothScan() {
        btController.refreshBTDevices();
    }

    /*TODO to trzeba sprawdzić*/
    @Receiver(actions = Intent.ACTION_HEADSET_PLUG)
    protected void onHeadsetPlugged(Intent intent) {
        int state = intent.getIntExtra("state", -1);
        switch(state) {
            case(0):
                Log.d("HeadSet", "Headset unplugged");
                break;
            case(1):
                Log.d("HeadSet", "Headset plugged");
                break;
            default:
                Log.d("HeadSet", "Error");
        }
    }

    @Subscribe
    public void onCorypheShutDown(CorypheShutDown event) {
        this.finishAffinity();
    }
}