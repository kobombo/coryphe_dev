package com.dominion.coryphe.bus;


import com.squareup.otto.Bus;

import org.androidannotations.annotations.EBean;

/**
 * @author dotreba.
 */
@EBean(scope = EBean.Scope.Singleton)
public class OttoBus extends Bus {

}
