package com.dominion.coryphe.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import com.dominion.coryphe.bus.OttoBus;
import com.dominion.coryphe.events.CorypheShutDown;
import com.dominion.coryphe.events.HeadsetPlugged;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dotreba.
 */
@EBean(scope = EBean.Scope.Singleton)
public class BlueToothController {

    @Bean
    OttoBus bus;
    private BluetoothAdapter btAdapter;
    private List<BluetoothDevice> boundedDevs;

    @AfterInject
    void init() {
        boundedDevs = new ArrayList<>();
        bus.register(this);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void startBT() {
        btAdapter.enable();
    }

    public void stopBT() {
        btAdapter.disable();
    }

    public void checkBtStatus() {
        if (btAdapter.getState() == BluetoothAdapter.STATE_OFF) {
            bus.post(new CorypheShutDown());
        }
    }

    public void refreshBTDevices() {
        boundedDevs.clear();
        boundedDevs.addAll(btAdapter.getBondedDevices());

        /*TODO do zmiany*/
        bus.post(new HeadsetPlugged());
    }

    private String getAliasName(BluetoothDevice device) {
        String deviceAlias = device.getName();
        try {
            Method method = device.getClass().getMethod("getAliasName");
            if (method != null) {
                deviceAlias = (String) method.invoke(device);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            return deviceAlias;
        }
    }

    public boolean isDeviceFound() {
        return boundedDevs != null && boundedDevs.size() != 0;
    }
}
