package com.dominion.coryphe.notification;

import android.content.Context;
import android.content.Intent;

import com.dominion.coryphe.bus.OttoBus;
import com.dominion.coryphe.events.HeadsetPlugged;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

/**
 * @author dotreba.
 */
@EBean(scope = EBean.Scope.Singleton)
public class NotificationController {

    private final Context context;

    @Bean
    OttoBus bus;

    public NotificationController(Context context){
        this.context=context;
    }

    @AfterInject
    void init(){
        bus.register(this);
    }

    @Subscribe
    public void onHeadsetPlugged(HeadsetPlugged event){
        try {
            NotificationService_.intent(context)/*.flags(Intent.FLAG_ACTIVITY_NEW_TASK)*/.start();
        }catch (RuntimeException ex){
            ex.printStackTrace();
        }
    }
}
